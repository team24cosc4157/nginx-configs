server {

    listen [::]:443 ssl;
    listen 443 ssl;

    server_name demand.team24.softwareengineeringii.com;

    underscores_in_headers on;

    location /api/backend {

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;

        proxy_pass http://localhost:4024/;
    }

    location /api/cs {

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        proxy_set_header X-Forwarded-Proto $scheme;

        proxy_pass http://localhost:4124/;
    }

    location /fe/ {

        root /home/team24;
        index index.html index.htm;
    }

    location / {

        root /home/team24/cs;
        index index.html index.htm;
    }

    ssl_certificate /etc/letsencrypt/live/demand.team24.softwareengineeringii.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/demand.team24.softwareengineeringii.com/privkey.pem;
}

server {

    listen 80 ;
    listen [::]:80 ;
    server_name demand.team24.softwareengineeringii.com;

    return 301 https://$host$request_uri;
}
