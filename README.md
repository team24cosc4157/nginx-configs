* `demand.team24.softwareengineeringii.com`
    * Nginx config file for demand-side server.

* `supply.team24.softwareengineeringii.com`
    * Nginx config file for supply-side server.
